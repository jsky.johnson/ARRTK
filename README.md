
[![Pipeline Status](https://gitlab.com/nullmage/ARRTK/badges/master/pipeline.svg)](https://gitlab.com/nullmage/ARRTK/commits/master) master

[![Pipeline Status](https://gitlab.com/nullmage/ARRTK/badges/next/pipeline.svg)](https://gitlab.com/nullmage/ARRTK/commits/next) next

# ARRTK
AR and Robotics Tool kit,

Here is a work in progress project that will be a collection of tools and assets to rapidly develop tests within a VR/AR Unity and Ros space
