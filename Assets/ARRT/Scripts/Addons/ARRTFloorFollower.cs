﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARRTFloorFollower : MonoBehaviour
{
    public GameObject followObject;
    // Start is called before the first frame update

    // Update is called once per frame
    void LateUpdate()
    {
        this.transform.SetPositionAndRotation(new Vector3(followObject.transform.position.x, GameObject.FindGameObjectWithTag("ROSWorld").transform.position.y, followObject.transform.position.z), Quaternion.Euler(90, 90 + followObject.transform.rotation.eulerAngles.y, 0));
    }
}
