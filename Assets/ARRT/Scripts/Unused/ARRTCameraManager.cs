﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARRTCameraManager : MonoBehaviour{
    public string Topic;
    private Vector3 recievedPosition;
    private Quaternion recievedQuaternion;
    private bool firstupdate = true;
    private bool recPos = false;


    private Vector3 offsetVect;
    // Start is called before the first frame update

    private void Start() {
        offsetVect = Vector3.zero;
    }


    private void Update() {

        if (firstupdate && recPos) {
            offsetVect = recievedPosition;// + transform.position;
            firstupdate = false;

            transform.rotation = recievedQuaternion;
        }

        //transform.rotation = recievedQuaternion;
        //Debug.Log("Current Camera Position: " + transform.position);
        //this.GetComponent<Transform>().position = recievedPosition;
        //this.GetComponent<Transform>().rotation = recievedQuaternion;
    }

    public void SetPosition(Vector3 rPos) {
        recPos = true;

        recievedPosition = new Vector3(rPos.x, rPos.z, rPos.y);
        //Debug.Log("Recieved Camera position " + recievedPosition + " current camera position " + transform.position);
        //recievedPosition = rPos;
        //offsetVect = rPos;
    }

    public void SetRotation(Quaternion rQuat) {

        Quaternion newQuat = new Quaternion(rQuat.x, rQuat.z, rQuat.y, rQuat.w);

        recievedQuaternion = Quaternion.Euler(new Vector3(newQuat.eulerAngles.x, newQuat.eulerAngles.y, newQuat.eulerAngles.z));

        Debug.Log("Recieved Camera quaternion " + rQuat.eulerAngles + " current Camera quaternion " + transform.rotation.eulerAngles);
        //recievedQuaternion = rQuat;
    }

    public Vector3 GetPosition() {
        return recievedPosition;
    }

    public Quaternion GetRotation() {
        return recievedQuaternion;
    }

    public Vector3 GetOffsetVector() {
        return offsetVect;
    }

    public void SetQuatAngle(Vector3 rEular) {
        this.GetComponent<Transform>().eulerAngles.Set(rEular.x, rEular.y, rEular.z);
    }
}
