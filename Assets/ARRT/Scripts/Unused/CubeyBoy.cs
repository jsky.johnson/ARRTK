﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeyBoy : MonoBehaviour {



    private ROSBridgeLib.ROSBridgeWebSocketConnection ros = null;
    // Start is called before the first frame update
    void Start() {
        ros = new ROSBridgeLib.ROSBridgeWebSocketConnection("ws://192.168.10.126", 9090);

        // Add subscribers and publishers (if any)
        //ros.AddSubscriber(typeof(CubeSub));
        //ros.AddPublisher(typeof(BallControlPublisher));

        // Fire up the subscriber(s) and publisher(s)
        ros.Connect();
    }

    // Update is called once per frame
    void Update() {
        ros.Render();
    }

    void OnApplicationQuit() {
        if (ros != null) {
            ros.Disconnect();
        }
    }
}
