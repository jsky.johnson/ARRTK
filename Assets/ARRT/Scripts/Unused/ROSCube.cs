﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ROSCube : MonoBehaviour{
    public string Topic;
    private Vector3 recievedPosition;
    private Quaternion recievedQuaternion;

    private GameObject cameraObject;

    private bool firstupdate = true;
    private bool recPos = false;

    public void Start() {
        cameraObject = GameObject.FindGameObjectWithTag("MainCamera");
    }

    private void Update() {

        if (firstupdate && recPos) {

            
            //offsetVect = recievedPosition;// + transform.position;
            firstupdate = false;

            //transform.rotation = recievedQuaternion;
        }


        //transform.RotateAround(new Vector3(cameraObject.transform.position.x, 0, cameraObject.transform.position.z), Vector3.up, 90 - cameraObject.transform.eulerAngles.y);
        // Debug.Log("Current Position: " + transform.rotation + "ViconPosition: " + recievedQuaternion);
        this.GetComponent<Transform>().position = recievedPosition - cameraObject.GetComponent<ARRTCameraManager>().GetOffsetVector();
        //this.GetComponent<Transform>().rotation = recievedQuaternion;

        //transform.RotateAround(cameraObject.GetComponent<ARRTCameraManager>().GetOffsetVector(), Vector3.up, 90); ;
        Debug.Log(cameraObject.GetComponent<ARRTCameraManager>().GetOffsetVector() +" Off");
        /*
        if (isFirstUpdate){// && hasHololensLocation) {
            adjustVec -= GameObject.FindGameObjectWithTag("MainCamera").GetComponent<ARRTCameraManager>().GetPosition(); ; //Hololens Location?
            adjustVecHasBeenSet = true;
            isFirstUpdate = false;
        }

        this.transform.position = adjustVec + recievedPosition;
        rotation_Object1.x = 0;
        rotation_Object1.z = 0;
        this.transform.rotation = Quaternion.Euler(recievedQuaternion.eulerAngles);

        */
        ////this.GetComponent<Transform>().position = recievedPosition;
        //this.GetComponent<Transform>().rotation = recievedQuaternion;
    }

    public void SetPosition(Vector3 rPos) {
        //Debug.Log(rPos);
        recievedPosition = new Vector3(rPos.x, rPos.z, rPos.y);
        //Debug.Log("Recieved Cube position " + recievedPosition + " current cube position " + transform.position);
    }

    public void SetRotation(Quaternion rQuat) {
        recievedQuaternion = new Quaternion(rQuat.x, rQuat.y, rQuat.z, rQuat.w);

        //Debug.Log("Recieved Cube quaternion " + recievedQuaternion.eulerAngles + " current cube quaternion " + transform.rotation);
    }
}
