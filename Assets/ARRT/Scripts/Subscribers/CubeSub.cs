﻿using SimpleJSON;
using UnityEngine;

public class CubeSub : ROSBridgeLib.ROSBridgeSubscriber {
    public new static string GetMessageTopic() {
        return "Cube1";//GameObject.FindGameObjectWithTag("Cube1").GetComponent<ROSCube>().Topic;
    }

    public new static string GetMessageType() {
        return "geometry_msgs/TransformStamped";
    }

    // Important function (I think, converting json to PoseMsg)
    public new static ROSBridgeMsg ParseMessage(JSONNode msg) {
        return new ROSBridgeLib.geometry_msgs.TransformStampedMsg(msg);
    }

    // This function should fire on each ros message
    public new static void CallBack(ROSBridgeMsg msg) {
        ROSBridgeLib.geometry_msgs.TransformStampedMsg thisMessage = (ROSBridgeLib.geometry_msgs.TransformStampedMsg) msg;

        float x = thisMessage.GetTransform().GetTranslation().GetX();
        float y = thisMessage.GetTransform().GetTranslation().GetY();
        float z = thisMessage.GetTransform().GetTranslation().GetZ();

        float qx = thisMessage.GetTransform().GetRotation().GetX();
        float qy = thisMessage.GetTransform().GetRotation().GetY();
        float qz = thisMessage.GetTransform().GetRotation().GetZ();
        float qw = thisMessage.GetTransform().GetRotation().GetW();

        GameObject Cube = GameObject.FindGameObjectWithTag("Cube1");

        Vector3 Position = new Vector3(x, y, z);
        Cube.transform.position = Position;
        Cube.transform.rotation = new Quaternion(qx, qy, qz, qw);

        //Debug.Log(Position);
    }
}
