﻿using System.Collections;
using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * This defines a subscriber. Subscribers listen to publishers in ROS. Now if we could have inheritance
 * on static classes then we could do this differently. But basically, you have to make up one of these
 * for every subscriber you need.
 * 
 * Subscribers require a ROSBridgePacket to subscribe to (its type). They need the name of
 * the message, and they need something to draw it. 
 * 
 * Version History
 * 3.1 - changed methods to start with an upper case letter to be more consistent with c#
 * style.
 * 3.0 - modification from hand crafted version 2.0
 * 
 * @author Michael Jenkin, Robert Codd-Downey and Andrew Speers
 * @version 3.1
 */


public class TextSub : ROSBridgeLib.ROSBridgeSubscriber {
    public TextMesh textmesh;

    public static string thisMessage;

    // These two are important
    public new static string GetMessageTopic() {
        return "text";
    }
    
    public new static string GetMessageType() {
        return "std_msgs/String";
    }

    // Important function (I think, converting json to PoseMsg)
    public new static ROSBridgeMsg ParseMessage(JSONNode msg) {
        return new ROSBridgeLib.std_msgs.StringMsg (msg);
    }

    // This function should fire on each ros message

    public new static void CallBack(ROSBridgeMsg msg) {

        ROSBridgeLib.std_msgs.StringMsg newMessage = (ROSBridgeLib.std_msgs.StringMsg) msg;

        Debug.Log(msg.ToString());
        //GameObject.FindGameObjectWithTag("TextTest").GetComponent<TextMesh>().text = msg.ToString();
    }
}

