﻿using SimpleJSON;
using UnityEngine;

public class ROSObject1Sub : ROSBridgeLib.ROSBridgeSubscriber
{
    public new static string GetMessageTopic()
    {
        return ROSConnectionManager.ROSObject1Topic_static;
    }

    //Ideally just add's transforms
    public new static string GetMessageType()
    {
        return "geometry_msgs/TransformStamped";
    }

    // Important function (I think, converting json to PoseMsg)
    public new static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new ROSBridgeLib.geometry_msgs.TransformStampedMsg(msg);
    }

    // This function should fire on each ros message
    public new static void CallBack(ROSBridgeMsg msg)
    {
        ROSBridgeLib.geometry_msgs.TransformStampedMsg incomingMessage = (ROSBridgeLib.geometry_msgs.TransformStampedMsg)msg;
        GameObject ROSObjectDestination = GameObject.FindGameObjectWithTag("ROS1");
        ROSObjectDestination.GetComponent<ROSObject>().StampedTransformHandler(incomingMessage);
    }
}
