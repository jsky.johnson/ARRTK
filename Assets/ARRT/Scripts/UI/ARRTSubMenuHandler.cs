﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARRTSubMenuHandler : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject ROSWorldMenu;
    public GameObject ROSObject1Menu;

    private bool ROSWorldStatus;
    private bool ROSObject1Status;
    void Start()
    {
        ROSWorldMenu.GetComponent<ARRTShowHideHandler>().HideObjectAndChildren();
        ROSObject1Menu.GetComponent<ARRTShowHideHandler>().HideObjectAndChildren();

        ROSWorldStatus = false;
        ROSObject1Status = false;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void HandleTurnOnROSWorld() {
        if(ROSWorldStatus == false) {
            
            ROSObject1Status = false;
            ROSObject1Menu.GetComponent<ARRTShowHideHandler>().HideObjectAndChildren();

            ROSWorldStatus = true;
            ROSWorldMenu.GetComponent<ARRTShowHideHandler>().ShowObjectAndChildren();

        } else {
            ROSWorldMenu.GetComponent<ARRTShowHideHandler>().HideObjectAndChildren();
            ROSObject1Menu.GetComponent<ARRTShowHideHandler>().HideObjectAndChildren();

            ROSWorldStatus = false;
            ROSObject1Status = false;
        }
    }

    public void HandleTurnOnROSObject1() {
        if (ROSObject1Status == false) {

            ROSWorldStatus = false;
            ROSWorldMenu.GetComponent<ARRTShowHideHandler>().HideObjectAndChildren();

            ROSObject1Status = true;
            ROSObject1Menu.GetComponent<ARRTShowHideHandler>().ShowObjectAndChildren();

        } else {
            ROSWorldMenu.GetComponent<ARRTShowHideHandler>().HideObjectAndChildren();
            ROSObject1Menu.GetComponent<ARRTShowHideHandler>().HideObjectAndChildren();

            ROSWorldStatus = false;
            ROSObject1Status = false;
        }
    }

    public void HandleMassOff() {
        ROSWorldMenu.GetComponent<ARRTShowHideHandler>().HideObjectAndChildren();
        ROSObject1Menu.GetComponent<ARRTShowHideHandler>().HideObjectAndChildren();

        ROSWorldStatus = false;
        ROSObject1Status = false;
    }
}
