﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Designed to handle all Gaze interactions within the ARRT 
 */
public class ARRTGazeHandler : MonoBehaviour
{

    public GameObject ObjectToChange;

    private Renderer ToChangeRenderer = null;
    public Material OnGazeMaterial;
    public Material OffGazeMaterial;
    // Start is called before the first frame update
    void Start()
    {
        ToChangeRenderer = ObjectToChange.GetComponent<Renderer>();
        ToChangeRenderer.material = OffGazeMaterial;
    }

    public void OnGazeChangeMaterial() {
        ToChangeRenderer.material = OnGazeMaterial;
    }

    public void OffGazeChangeMaterial() {
        ToChangeRenderer.material = OffGazeMaterial;
    }
}
