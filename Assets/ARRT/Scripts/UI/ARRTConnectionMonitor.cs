﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARRTConnectionMonitor : MonoBehaviour
{
    public GameObject ROSObject;
    public TMPro.TextMeshPro ConnectionText;

    public GameObject ConnectionIcon;
    public Material GoodConnection;
    public Material MediumConnection;
    public Material BadConnection;
    public Material Disconnection;
    public Material NoConnection;

    private int ConnectionStatus;
    // Start is called before the first frame update
    void Start()
    {
        ConnectionStatus = ROSObject.GetComponent<ROSObject>().GetConnectionStatus();
    }

    // Update is called once per frame
    void Update()
    {
        ConnectionStatus = ROSObject.GetComponent<ROSObject>().GetConnectionStatus();

        switch (ConnectionStatus) {
            case -1:
                ConnectionIcon.GetComponent<Renderer>().material = NoConnection;
                break;
            case 0:
                ConnectionIcon.GetComponent<Renderer>().material = GoodConnection;
                break;
            case 1:
                ConnectionIcon.GetComponent<Renderer>().material = MediumConnection;
                break;
            case 2:
                ConnectionIcon.GetComponent<Renderer>().material = BadConnection;
                break;
            case 3:
                ConnectionIcon.GetComponent<Renderer>().material = Disconnection;
                break;
        }

        ConnectionText.text = ROSObject.GetComponent<ROSObject>().GetConnectionLatency().ToString() + " :ms";
    }
}
