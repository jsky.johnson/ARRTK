﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class ConnectionButton : MonoBehaviour {
    // Start is called before the first frame update

    public TouchScreenKeyboard keyboard;
    public static string keyboardText = "";

    private string connectionString = "";

    private bool connectingValue;

    public GameObject ROSConnectionManager;
    public TMPro.TextMeshPro inputText;
    public TMPro.TextMeshPro connectionText;

    public Material Connect;
    public Material Disconnect;

    public GameObject ConnectionButtonGameObject;

    //public TMPro.TextMeshPro thisButtonText;
    //public TMPro.TextMeshPro placeHolderText;

    void Start() {
        connectingValue = true;
    }

    // Update is called once per frame
    void Update() {

        if (keyboard != null) {
            keyboardText = keyboard.text;
            if (TouchScreenKeyboard.visible) {
                inputText.text = keyboardText;
            } else {
                inputText.text = keyboardText;
                keyboard = null;
            }
        }
    }

    public void ButtonPressed() {
        Debug.Log("Button Pressed!");
        if (connectingValue) {
            ConnectPress();
        } else {
            DisconnectPress();
        }
    }

    void ConnectPress() {

        if (CheckIPValid(inputText.text)) {
            connectionString = inputText.text;
            Debug.Log("Connecting to : " + inputText.text);
            ROSConnectionManager.GetComponent<ROSConnectionManager>().SetAConnection(connectionString);
            
        } else {
            connectionString = "192.168.10.113";
            Debug.Log("Non-valid IP entered, Connecting to : " + inputText.text);
            ROSConnectionManager.GetComponent<ROSConnectionManager>().SetAConnection(connectionString);
        }

        //thisButtonText.text = "Disconnect/Reset";
        connectingValue = false;
        ConnectionButtonGameObject.GetComponent<Renderer>().material = Disconnect;
        connectionText.text = "Disconnect";
    }

    void DisconnectPress() {
        ROSConnectionManager.GetComponent<ROSConnectionManager>().Disconnect();

        //thisButtonText.text = "Connect To: ";
        connectingValue = true;
        ConnectionButtonGameObject.GetComponent<Renderer>().material = Connect;
        connectionText.text = "Connect";
    }

    public static bool CheckIPValid(string strIP) {
        IPAddress result = null;
        return
            !string.IsNullOrEmpty(strIP) &&
            IPAddress.TryParse(strIP, out result);
    }

    public void OpenSystemKeyboard() {
        keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.NumbersAndPunctuation, false, false, false, false);
    }
}
