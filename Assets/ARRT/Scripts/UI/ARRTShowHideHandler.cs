﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARRTShowHideHandler : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject[] Children;

    private bool hidden;

    public bool ActiveOnStart;

    void Start()
    {
        if (ActiveOnStart) {
            hidden = false;
        } else {
            hidden = true;
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ToggleHideObjectAndChildren() {
        if (hidden) {

            foreach (GameObject child in Children) {

                

                if (child.GetComponent<ARRTShowHideHandler>().ActiveOnStart) {
                    child.GetComponent<ARRTShowHideHandler>().ShowObjectAndChildren();
                } else {
                    child.GetComponent<ARRTShowHideHandler>().HideObjectAndChildren();
                }
                
            }

            this.gameObject.SetActive(true);
            hidden = false;

            
        } else {
            foreach (GameObject child in Children) {
                if (child.GetComponent<ARRTSubMenuHandler>() == null) {

                } else {
                    child.GetComponent<ARRTSubMenuHandler>().HandleMassOff();
                }

                child.GetComponent<ARRTShowHideHandler>().HideObjectAndChildren();
            }

            this.gameObject.SetActive(false);
            hidden = true;

            
        }
    }

    public void HideObjectAndChildren() {
        foreach (GameObject child in Children) {
            child.SetActive(false);
        }

        this.gameObject.SetActive(false);
        hidden = true;
    }

    public void ShowObjectAndChildren() {
        foreach (GameObject child in Children) {
            child.SetActive(true);
        }

        this.gameObject.SetActive(true);
        hidden = false;
    }

}
