﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARRTOffset : MonoBehaviour
{
    private ROSObject rosOb;
    public GameObject gameOb;
    // Start is called before the first frame update
    void Start() {
        rosOb = gameOb.GetComponent<ROSObject>();
    }

    // Update is called once per frame
    void Update() {
        this.GetComponent<TMPro.TextMeshPro>().text = "Offset : " + rosOb.GetOffset().ToString();
    }
}
