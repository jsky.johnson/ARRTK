﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ROSObject : MonoBehaviour
{

    public string topic;
    // Start is called before the first frame update

    private double lastTime;


    System.DateTime epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
    //Connection Status Values (temp)
    //-1 = not connected, 0 = good, 1 = poor, 2 = bad, 3 = disconnected

    private GameObject ROSWorld;

    public bool staticObject;

    private float ModRate = 0.5f;
    private bool RotationMod = false;


    //Connection
    private float Connectionlatency;
    private int ConnectionStatus;

    //MovementAndMessage related Variables


    public GameObject OffsetObject;
    private Vector3 OffsetPosition;
    private Vector3 OffsetRotation;


    private Vector3 IncomingPosition;
    private Vector3 CurrentPosition;
    private Vector3 PreviousPosition;

    private Vector3 IncomingRotation;
    private Vector3 CurrentRotation;
    private Vector3 PreviousRotation;

    private double LastUpdateTime;
    private ROSBridgeLib.geometry_msgs.TransformStampedMsg PreviousMessage;

    [SerializeField]
    [Header("Filtering")]
    public bool ShowOriginal;
    public GameObject UnfilteredObject;
    private GameObject unfilteredObjectProjection;

    [Header("Predictive")]
    [SerializeField]
    public bool Kalman;
    [SerializeField] [Range(0, 4)] public float Q_measurementNoise;// measurement noise
    [SerializeField] [Range(0, 100)] public float R_EnvironmentNoize; // environment noise
    [SerializeField] [Range(0, 100)] public float F_facorOfRealValueToPrevious; // factor of real value to previous real value
    [SerializeField] [Range(0.1f, 100)] public float H_factorOfMeasuredValue; // factor of measured value to real value
    [SerializeField] public float m_StartState;
    [SerializeField] public float m_Covariance;

    private KFilter filterx;
    private KFilter filtery;
    private KFilter filterz;

    [Header("Non-Predictive")]
    [Space]
    [SerializeField]
    public bool Dampening;
    [SerializeField] [Range(0.001f, 2.1f)] public float smoothTime = 0.18f;
    [SerializeField] [Range(0.001f, 2.1f)] public float rotationSmoothTime = 1.1f;
    private float xVelocity = 0.0f;
    private float yVelocity = 0.0f;
    private float zVelocity = 0.0f;

    private float xrVelocity = 0.0f;
    private float yrVelocity = 0.0f;
    private float zrVelocity = 0.0f;



    void Start()
    {


        //Debug.Log(positions.ToString());
        lastTime = (double)(System.DateTime.UtcNow - epochStart).TotalMilliseconds;
        CurrentPosition = this.transform.position;
        //lastTime = Time.time;
        if (!staticObject)
        {
            ROSWorld = GameObject.FindGameObjectWithTag("ROSWorld");
        }

        PreviousPosition = Vector3.zero;
        PreviousRotation = Vector3.zero;

        IncomingPosition = Vector3.zero;
        IncomingRotation = Vector3.zero;
        LastUpdateTime = 0;
    }

    void Update()
    {
        if (!staticObject)
        {
            OffsetPosition = OffsetObject.transform.localPosition + new Vector3(0, .3f, 0);
            OffsetRotation = OffsetObject.transform.rotation.eulerAngles;

            transform.position = CurrentPosition + OffsetPosition + ROSWorld.transform.position;

            Debug.Log("Transforms" + OffsetObject.transform.position + " " + OffsetPosition + " " + CurrentPosition + " " + ROSWorld.transform.position + " " + transform.position);
        }
    }

    //void FixedUpdate() {

    //}


    public void StampedTransformHandler(ROSBridgeLib.geometry_msgs.TransformStampedMsg incomingMessage)
    {

        float x = incomingMessage.GetTransform().GetTranslation().GetX();
        float y = incomingMessage.GetTransform().GetTranslation().GetY();
        float z = incomingMessage.GetTransform().GetTranslation().GetZ();

        float qx = incomingMessage.GetTransform().GetRotation().GetX();
        float qy = incomingMessage.GetTransform().GetRotation().GetY();
        float qz = incomingMessage.GetTransform().GetRotation().GetZ();
        float qw = incomingMessage.GetTransform().GetRotation().GetW();

        //Debug.Log(incomingMessage.GetHeader().ToString());
        double sysTime = (double)(System.DateTime.UtcNow - epochStart).TotalMilliseconds;

        double ROSTime = ROSTimeToFloat(incomingMessage.GetHeader().GetTimeMsg().GetSecs(), incomingMessage.GetHeader().GetTimeMsg().GetNsecs());
        //Debug.Log("ROSTime" + ROSTime + " SysTime : " + sysTime + "difference : " + (sysTime - ROSTime)); 
        //Debug.Log(incomingMessage.GetHeader().GetTimeMsg().GetSecs().ToString() + " : " +ROSTime+ " : " +sysTime + " : " + (sysTime - ROSTime));
        //Add first 5 nanoseconds to newTime to get them aligned?


        //Adds new Positioning
        IncomingPosition = new Vector3(x, z, y); //From VICON
        IncomingRotation = new Quaternion(qx, qy, qz, qw).eulerAngles; //From VICON
        Vector3 DiffCurrentPosition = IncomingPosition - PreviousPosition;
        Vector3 DiffCurrentRotation = IncomingRotation - PreviousRotation;


        if (Kalman)
        {

            filterx = new KFilter(RealValueToPreviousRealValue: F_facorOfRealValueToPrevious, MeasuredToRealValue: H_factorOfMeasuredValue, mesurementNoize: Q_measurementNoise, environmentNoize: R_EnvironmentNoize);
            filtery = new KFilter(RealValueToPreviousRealValue: F_facorOfRealValueToPrevious, MeasuredToRealValue: H_factorOfMeasuredValue, mesurementNoize: Q_measurementNoise, environmentNoize: R_EnvironmentNoize);
            filterz = new KFilter(RealValueToPreviousRealValue: F_facorOfRealValueToPrevious, MeasuredToRealValue: H_factorOfMeasuredValue, mesurementNoize: Q_measurementNoise, environmentNoize: R_EnvironmentNoize);

            filterx.State = this.transform.position.x;
            filtery.State = this.transform.position.y;
            filterz.State = this.transform.position.z;

            float FilteredValuex = filterx.FilterValue(DiffCurrentPosition.x);
            float FilteredValuey = filterx.FilterValue(DiffCurrentPosition.y);
            float FilteredValuez = filterx.FilterValue(DiffCurrentPosition.z);

            Vector3 FitleredPos = new Vector3(FilteredValuex, FilteredValuey, FilteredValuez);
            Debug.Log(FitleredPos);
            this.transform.position = FitleredPos;
        }
        else if (Dampening)
        {

            float newPositiony = Mathf.SmoothDamp(transform.position.y, CurrentPosition.y, ref xVelocity, smoothTime);
            float newPositionx = Mathf.SmoothDamp(transform.position.x, CurrentPosition.x, ref yVelocity, smoothTime);
            float newPositionz = Mathf.SmoothDamp(transform.position.z, CurrentPosition.z, ref zVelocity, smoothTime);
            transform.localPosition = new Vector3(newPositionx, newPositiony, newPositionz);

            //float newRotationX = Mathf.SmoothDamp(transform.rotation.eulerAngles.x, CurrentRotation.x, ref xrVelocity, smoothTime);
            //float newRotationY = Mathf.SmoothDamp(transform.rotation.eulerAngles.y, CurrentRotation.y, ref yrVelocity, smoothTime);
            //float newRotationZ = Mathf.SmoothDamp(transform.rotation.eulerAngles.z, CurrentRotation.z, ref zrVelocity, smoothTime);
            transform.localRotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(CurrentRotation), rotationSmoothTime);//Quaternion.Euler(new Vector3(newRotationX, newRotationY, newRotationZ));

            //this.transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles + DiffRot);

        }
        else
        {
            //this.transform.SetPositionAndRotation(CurrentPosition, Quaternion.Euler(CurrentRotation));
            CurrentPosition = IncomingPosition;// + OffsetPosition;
            this.transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles + DiffCurrentRotation);
        }

        if (ShowOriginal)
        {
            if (unfilteredObjectProjection == null)
            {
                unfilteredObjectProjection = Instantiate(UnfilteredObject, CurrentPosition, Quaternion.Euler(CurrentRotation));
            }
            else
            {
                unfilteredObjectProjection.transform.SetPositionAndRotation(CurrentPosition, Quaternion.Euler(CurrentRotation));
            }

        }
        else if (!ShowOriginal)
        {
            if (unfilteredObjectProjection != null)
            {
                Destroy(unfilteredObjectProjection);
            }
        }

        //End of Call
        PreviousRotation = IncomingRotation;
        PreviousPosition = IncomingPosition;
        lastTime = sysTime;
        PreviousMessage = incomingMessage;
    }

    public int GetConnectionStatus()
    {
        return ConnectionStatus;
    }

    public float GetConnectionLatency()
    {
        return Connectionlatency;
    }

    private double ROSTimeToFloat(int secs, int nsecs)
    {
        //first 5 from Nanoseconds
        char[] nSecChar = nsecs.ToString().ToCharArray();
        //Debug.Log(nSecChar.Length);

        //Debug.Log(secs + " " + nsecs);
        string nsecString = "";
        if (nSecChar.Length == 9)
        {
            nsecString = nSecChar[0] + nSecChar[1] + nSecChar[2] + "." + nSecChar[3] + nSecChar[4];
        }
        else
        {
            nsecString = "0" + nSecChar[0] + nSecChar[1] + "." + nSecChar[2] + nSecChar[3];
        }

        string newString = secs.ToString() + nsecString;

        return double.Parse(newString);
    }

    public void Recenter()
    {
        if (RotationMod)
        {
            this.transform.rotation = Quaternion.identity;
        }
        else
        {
            this.transform.position = Vector3.zero;
        }
    }

    public void Up()
    {
        if (RotationMod)
        {
            this.transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles + new Vector3(0, ModRate, 0));
        }
        else
        {
            this.transform.position += new Vector3(0, ModRate, 0);
        }
    }

    public void Down()
    {

        if (RotationMod)
        {
            this.transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles + new Vector3(0, -ModRate, 0));
        }
        else
        {
            this.transform.position += new Vector3(0, -ModRate, 0);
        }
    }

    public void Left()
    {

        if (RotationMod)
        {
            this.transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles + new Vector3(0, 0, -ModRate));
        }
        else
        {
            this.transform.position += new Vector3(0, 0, -ModRate);
        }
    }

    public void Right()
    {
        if (RotationMod)
        {
            this.transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles + new Vector3(0, 0, ModRate));
        }
        else
        {
            this.transform.position += new Vector3(0, 0, ModRate);
        }
    }

    public void Forward()
    {

        if (RotationMod)
        {
            this.transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles + new Vector3(ModRate, 0, 0));
        }
        else
        {
            this.transform.position += new Vector3(ModRate, 0, 0);
        }
    }

    public void Backward()
    {

        if (RotationMod)
        {
            this.transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles + new Vector3(-ModRate, 0, 0));
        }
        else
        {
            this.transform.position += new Vector3(-ModRate, 0, 0);
        }
    }

    public void IncScale()
    {
        ModRate *= 2;
    }

    public void DecScale()
    {
        ModRate /= 2;
    }

    public void RotateToggle()
    {
        if (RotationMod)
        {
            RotationMod = false;
            ModRate = .1f;
        }
        else
        {
            RotationMod = true;
            ModRate = 3.2f;
        }
    }

    public Vector3 GetOffset()
    {
        return OffsetPosition;
    }
    public bool GetRotationMod()
    {
        return RotationMod;
    }
    public Vector3 GetOffsetRotation()
    {
        return OffsetRotation;
    }
    public float GetModRate()
    {
        return ModRate;
    }
}
