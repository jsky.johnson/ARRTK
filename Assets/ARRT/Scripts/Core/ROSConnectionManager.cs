﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Designed to handle connection, disconnections, and subscribing, and services with ROS
 */
public class ROSConnectionManager : MonoBehaviour
{
    private ROSBridgeLib.ROSBridgeWebSocketConnection ros = null;

    //private string IP = "192.168.10.108";

    private bool connected = false;

    private bool addSub1 = false;
    public string IP_out = "192.168.10.108";

    [Tooltip("Generally 9090, but maybe 11311 for Vicon bridge?")]
    public int IP_port = 9090;
    public bool ConnectOnStart = false;

    [Header("Static Fields")]
    [SerializeField] public string ROSObject1Topic;

    public static string ROSObject1Topic_static = "";

    void Start()
    {
        ROSObject1Topic_static = ROSObject1Topic;
        if (ConnectOnStart)
        {
            SetAConnection(IP_out);
        }

    }

    // Update is called once per frame
    void Update()
    {

        if (ros != null)
        {
            ros.Render();
        }

        if (connected)
        {
            if (addSub1)
            {
                Debug.Log("AddingCube");
                AddROSObject1();
                addSub1 = false;


            }
        }


    }

    void OnApplicationQuit()
    {
        if (ros != null)
        {
            Disconnect();
        }
    }

    public void SetAConnection(string IP)
    {
        ros = new ROSBridgeLib.ROSBridgeWebSocketConnection("ws://" + IP, IP_port); //"ws://192.168.10.126"
        Connect();
    }

    void Connect()
    {
        if (ros != null)
        {
            connected = true;

            ros.Connect();
            Debug.Log("Connected " + connected);
            addSub1 = true;
        }
    }
    public void Disconnect()
    {
        if (ros != null)
        {
            connected = false;
            ros.Disconnect();
            ros = null;
        }
        addSub1 = true;
    }


    public bool getConnection()
    {
        return connected;
    }
    /// <summary>
    /// I need to figure out how to add Subscribers Ambigiously???
    /// </summary>
    void AddROSObject1()
    {
        ros.AddSubscriber(typeof(ROSObject1Sub));
    }
}
